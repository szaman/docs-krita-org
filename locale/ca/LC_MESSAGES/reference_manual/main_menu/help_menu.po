# Translation of docs_krita_org_reference_manual___main_menu___help_menu.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 21:02+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "About KDE"
msgstr "Quant al KDE"

#: ../../reference_manual/main_menu/help_menu.rst:1
msgid "The help menu in Krita."
msgstr "El menú Ajuda en el Krita."

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "About"
msgstr "Quant al"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Handbook"
msgstr "Manual"

#: ../../reference_manual/main_menu/help_menu.rst:11
msgid "Bug"
msgstr "Error"

#: ../../reference_manual/main_menu/help_menu.rst:16
msgid "Help Menu"
msgstr "El menú Ajuda"

#: ../../reference_manual/main_menu/help_menu.rst:18
msgid "Krita Handbook"
msgstr "Manual del Krita"

#: ../../reference_manual/main_menu/help_menu.rst:19
msgid "Opens a browser and sends you to the index of this manual."
msgstr "Obre un navegador i obre l'índex d'aquest manual."

#: ../../reference_manual/main_menu/help_menu.rst:20
msgid "Report Bug"
msgstr "Informa d'un error"

#: ../../reference_manual/main_menu/help_menu.rst:21
msgid "Sends you to the bugtracker."
msgstr "Obre el seguidor d'errors."

#: ../../reference_manual/main_menu/help_menu.rst:22
msgid "Show system information for bug reports."
msgstr "Mostra la informació del sistema pels informes d'error."

#: ../../reference_manual/main_menu/help_menu.rst:23
msgid ""
"This is a selection of all the difficult to figure out technical information "
"of your computer. This includes things like, which version of Krita you "
"have, which version your operating system is, and most prudently, what kind "
"of OpenGL functionality your computer is able to provide. The latter varies "
"a lot between computers and due that it is one of the most difficult things "
"to debug. Providing such information can help us figure out what is causing "
"a bug."
msgstr ""
"Aquesta és una selecció de totes les dades tècniques difícils de determinar "
"del vostre ordinador. Això inclou coses com la versió del Krita, la versió "
"del sistema operatiu i, el més prudent, el tipus de funcionalitat OpenGL que "
"pot proporcionar. Això últim variarà molt entre els ordinadors i de fet és "
"una de les coses més difícils de depurar. Proporcionar la informació ens "
"ajudarà a descobrir què està causant un error."

#: ../../reference_manual/main_menu/help_menu.rst:24
msgid "About Krita"
msgstr "Quant al Krita"

#: ../../reference_manual/main_menu/help_menu.rst:25
msgid "Shows you the credits."
msgstr "Mostra els crèdits."

#: ../../reference_manual/main_menu/help_menu.rst:27
msgid "Tells you about the KDE community that Krita is part of."
msgstr "Explica quant a la comunitat KDE de la qual forma part el Krita."
