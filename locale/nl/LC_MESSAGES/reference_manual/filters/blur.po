# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 23:00+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/filters/Lens-blur-filter.png"
msgstr ".. image:: images/filters/Lens-blur-filter.png"

#: ../../reference_manual/filters/blur.rst:1
msgid "Overview of the blur filters."
msgstr "Overzicht van de filters voor vervagen."

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:15
#: ../../reference_manual/filters/blur.rst:39
msgid "Blur"
msgstr "Vervagen"

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:25
msgid "Gaussian Blur"
msgstr "Gaussiaans vervagen"

#: ../../reference_manual/filters/blur.rst:10
msgid "Filters"
msgstr "Filters"

#: ../../reference_manual/filters/blur.rst:17
msgid ""
"The blur filters are used to smoothen out the hard edges and details in the "
"images. The resulting image is blurry. below is an example of a blurred "
"image. The image of Kiki on right is the result of blur filter applied to "
"the image on left."
msgstr ""
"De filters voor vervagen worden gebruikt om de harde randen en details in de "
"afbeeldingen gladder te maken. De resulterende afbeelding is wazig. "
"Onderstaand is een voorbeeld van een wazig gemaakte afbeelding. De "
"afbeelding van Kiki rechts is het resultaat van filter voor vervaging "
"toegepast op de afbeelding links."

#: ../../reference_manual/filters/blur.rst:21
msgid ".. image:: images/filters/Blur.png"
msgstr ".. image:: images/filters/Blur.png"

#: ../../reference_manual/filters/blur.rst:22
msgid "There are many different filters for blurring:"
msgstr "Er zijn veel verschillende filters voor vervagen:"

#: ../../reference_manual/filters/blur.rst:27
msgid ""
"You can input the horizontal and vertical radius for the amount of blurring "
"here."
msgstr ""
"U kunt de horizontale en verticale straal voor de hoeveelheid vervanging "
"hier invoeren."

#: ../../reference_manual/filters/blur.rst:30
msgid ".. image:: images/filters/Gaussian-blur.png"
msgstr ".. image:: images/filters/Gaussian-blur.png"

#: ../../reference_manual/filters/blur.rst:32
msgid "Motion Blur"
msgstr "Bewegingsvervaging"

#: ../../reference_manual/filters/blur.rst:34
msgid ""
"Doesn't only blur, but also subtly smudge an image into a direction of the "
"specified angle thus giving a feel of motion to the image. This filter is "
"often used to create effects of fast moving objects."
msgstr ""
"Vervaagt niet alleen, maar smeert een afbeelding ook subtiel uit in een "
"richting van de gespecificeerde hoek waarmee de afbeelding een gevoel "
"gegeven wordt van beweging in de afbeelding. Dit filter wordt vaak gebruikt "
"om het effect van snel bewegende objecten te maken."

#: ../../reference_manual/filters/blur.rst:37
msgid ".. image:: images/filters/Motion-blur.png"
msgstr ".. image:: images/filters/Motion-blur.png"

#: ../../reference_manual/filters/blur.rst:41
msgid "This filter creates a regular blur."
msgstr "Dit filter maakt een reguliere vervaging."

#: ../../reference_manual/filters/blur.rst:44
msgid ".. image:: images/filters/Blur-filter.png"
msgstr ".. image:: images/filters/Blur-filter.png"

#: ../../reference_manual/filters/blur.rst:46
msgid "Lens Blur"
msgstr "Lensvervaging"

#: ../../reference_manual/filters/blur.rst:48
msgid "Lens Blur Algorithm."
msgstr "Algoritme voor lensvervaging."
