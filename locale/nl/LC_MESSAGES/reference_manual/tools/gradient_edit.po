# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 11:01+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../<rst_epilog>:18
msgid ""
".. image:: images/icons/gradient_edit_tool.svg\n"
"   :alt: toolgradientedit"
msgstr ""
".. image:: images/icons/gradient_edit_tool.svg\n"
"   :alt: toolgradientedit"

#: ../../reference_manual/tools/gradient_edit.rst:1
msgid "Krita's vector gradient editing tool reference."
msgstr ""
"Verwijzing naar hulpmiddel voor bewerken van verloop van vector van Krita."

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Gradient"
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:16
msgid "Gradient Editing Tool"
msgstr "Hulpmiddel voor bewerken van verloop"

#: ../../reference_manual/tools/gradient_edit.rst:18
msgid "|toolgradientedit|"
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:22
msgid ""
"This tool has been removed in Krita 4.0, and its functionality has been "
"folded into the :ref:`shape_selection_tool`."
msgstr ""

#: ../../reference_manual/tools/gradient_edit.rst:24
msgid ""
"This tool allows you to edit the gradient on canvas, but it only works for "
"vector layers. If you have a vector shape selected, and draw a line over the "
"canvas, you will be able to see the nodes, and the stops in the gradient. "
"Move around the nodes to move the gradient itself. Select the stops to "
"change their color in the tool options docker, or to move their position in "
"the on canvas gradient. You can select preset gradient in the tool docker to "
"change the active shape's gradient to use those stops."
msgstr ""
