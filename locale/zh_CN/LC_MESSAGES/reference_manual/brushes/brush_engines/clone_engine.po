msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___clone_engine."
"pot\n"

#: ../../<generated>:1
msgid "Clone from all visible layers"
msgstr ""

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: 鼠标左键"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:1
#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:18
msgid "The Clone Brush Engine manual page."
msgstr "介绍 Krita 的克隆笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:16
msgid "Clone Brush Engine"
msgstr "克隆笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:11
msgid "Clone Tool"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:30
msgid ".. image:: images/icons/clonebrush.svg"
msgstr ".. image:: images/icons/clonebrush.svg"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:31
msgid ""
"The clone brush is a brush engine that allows you to paint with a "
"duplication of a section of a paint-layer. This is useful in manipulation of "
"photos and textures. You have to select a source and then you can paint to "
"copy or clone the source to a different area. Other applications normally "
"have a separate tool for this, Krita has a brush engine for this."
msgstr ""
"此笔刷引擎可以绘制出一个颜料图层的某个区域的复制品，它在修改照片和纹理时非常"
"有用。使用此引擎的笔刷时要线选定一处源图像，然后便可以在别处将源图像原封不动"
"地进行重现。其他软件通常有一个专门的工具来实现这种操作，而 Krita 则是通过笔刷"
"引擎来实现。"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:34
msgid "Usage and Hotkeys"
msgstr "用法和快捷键"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:36
msgid ""
"To see the source, you need to set the brush-cursor settings to brush "
"outline."
msgstr ""
"在某些版本中，要在绘制时看见采样源的位置，你要将光标的轮廓形状设为“预览轮"
"廓” (默认值)。"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:38
msgid ""
"The clone tool can now clone from the projection and it's possible to change "
"the clone source layer. Press the :kbd:`Ctrl + Alt +` |mouseleft| shortcut "
"to select a new clone source on the current layer. The :kbd:`Ctrl +` |"
"mouseleft| shortcut to select a new clone source point on the layer that was "
"active when you selected the clone op."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:42
msgid ""
"The :kbd:`Ctrl + Alt +` |mouseleft| shortcut is temporarily disabled on "
"2.9.7."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:45
msgid "Settings"
msgstr "可用笔刷设置"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:47
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:48
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:49
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:52
msgid "Painting mode"
msgstr "绘画模式"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:54
msgid "Healing"
msgstr "修复"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:55
msgid ""
"This turns the clone brush into a healing brush: often used for removing "
"blemishes in photo retouching, and maybe blemishes in painting."
msgstr ""
"此选项会将克隆笔刷变为修复笔刷，一般用于在修改照片时用于移除污点等不理想的部"
"分，也可以用于画作。"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:57
msgid "Only works when there's a perspective grid visible."
msgstr "此选项只在透视网格可见时生效。"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:59
msgid "Perspective correction"
msgstr "透视修正"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:60
msgid "This feature is currently disabled."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:61
msgid "Source Point move"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:62
msgid ""
"This will determine whether you will replicate the source point per dab or "
"per stroke. Can be useful when used with the healing brush."
msgstr ""
"此选项决定每一条笔画复制来源点一次，还是每次笔尖印迹复制来源点一次。在使用修"
"复笔刷时可以发挥作用。"

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:63
msgid "Source Point reset before a new stroke"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:64
msgid ""
"This will reset the source point everytime you make a new stroke. So if you "
"were cloning a part in one stroke, having this active will allow you to "
"clone the same part again in a single stroke, instead of using the source "
"point as a permanent offset."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/clone_engine.rst:66
msgid ""
"Tick this to force cloning of all layers instead of just the active one."
msgstr "勾选此项后将从所有图层进行克隆，而不仅限于当前图层。"
