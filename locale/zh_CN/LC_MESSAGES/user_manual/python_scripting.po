msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___python_scripting.pot\n"

#: ../../user_manual/python_scripting.rst:5
msgid "Python Scripting"
msgstr "Python 脚本编程"

#: ../../user_manual/python_scripting.rst:7
msgid "This section covers python scripting."
msgstr "本章节将介绍 Krita 的 Python 脚本编程。"

#: ../../user_manual/python_scripting.rst:9
msgid "Contents:"
msgstr "目录："
