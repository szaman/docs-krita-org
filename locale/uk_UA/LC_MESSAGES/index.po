# Translation of docs_krita_org_index.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_index\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-05 08:03+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_userManual.jpg"
msgstr ".. image:: images/intro_page/Hero_userManual.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_tutorials.jpg"
msgstr ".. image:: images/intro_page/Hero_tutorials.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_getting_started.jpg"
msgstr ".. image:: images/intro_page/Hero_getting_started.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_reference.jpg"
msgstr ".. image:: images/intro_page/Hero_reference.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_general.jpg"
msgstr ".. image:: images/intro_page/Hero_general.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_faq.jpg"
msgstr ".. image:: images/intro_page/Hero_faq.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_resources.jpg"
msgstr ".. image:: images/intro_page/Hero_resources.jpg"

#: ../../index.rst:5
msgid "Welcome to the Krita |version| Manual!"
msgstr "Вітаємо на сторінці підручника з Krita |version|!"

#: ../../index.rst:7
msgid "Welcome to Krita's documentation page."
msgstr "Вітаємо на сторінці документації з Krita!"

#: ../../index.rst:9
msgid ""
"Krita is a sketching and painting program designed for digital artists. Our "
"vision for Development of Krita is —"
msgstr ""
"Krita — програма для створення ескізів і малюнків, яку розроблено для "
"цифрового малювання. Ось, як ми бачимо мету розробки Krita —"

#: ../../index.rst:11
msgid ""
"Krita is a free and open source cross-platform application that offers an "
"end-to-end solution for creating digital art files from scratch. Krita is "
"optimized for frequent, prolonged and focused use. Explicitly supported "
"fields of painting are illustrations, concept art, matte painting, textures, "
"comics and animations. Developed together with users, Krita is an "
"application that supports their actual needs and workflow. Krita supports "
"open standards and interoperates with other applications."
msgstr ""
"Krita є вільною програмою з відкритим кодом, яка працює на багатьох "
"програмних платформах і є універсальним рішенням для створення файлів "
"цифрових художніх творів з нуля. Krita оптимізовано для частого, тривалого "
"та фокусованого використання. Акцент зроблено на підтримці малювання "
"ілюстрацій, концепт-арту, домальовуванні на фотографіях, малюванні текстур, "
"коміксів та анімацій. Розроблена у співпраці із користувачами, Krita є "
"програмою, яка забезпечує задоволення потреб та робочих процесів "
"користувачів. У Krita передбачено підтримку відкритих стандартів та "
"взаємодії із іншими програмами."

#: ../../index.rst:19
msgid ""
"Krita's tools are developed keeping the above vision in mind. Although it "
"has features that overlap with other raster editors its intended purpose is "
"to provide robust tool for digital painting and creating artworks from "
"scratch. As you learn about Krita, keep in mind that it is not intended as a "
"replacement for Photoshop. This means that the other programs may have more "
"features than Krita for image manipulation tasks, such as stitching together "
"photos, while Krita's tools are most relevant to digital painting, concept "
"art, illustration, and texturing. This fact accounts for a great deal of "
"Krita's design."
msgstr ""
"Інструменти Krita розроблено з огляду на описану вище концепцію. Хоча "
"можливості програми можуть перекриватися із іншими редакторами растрової "
"графіки, основою метою цієї програми є надійне забезпечення роботи у режимі "
"цифрового малювання та створення художніх творів від початку. Вивчаючи "
"можливості Krita, пам'ятайте, що програму не створювали як замінник "
"Photoshop. Це означає, що інші програми можуть мати ширші можливості із "
"обробки зображень, ніж Krita, зокрема можуть зшивати фотографії у панораму. "
"Інструменти ж Krita підібрано так, щоб програма була найзручнішою для "
"цифрового малювання, створення концептуальних художніх творів, ілюстрування "
"та створення текстур. Це може у значній частині пояснити, чому компонування "
"Krita є саме таким як зараз."

#: ../../index.rst:28
msgid ""
"You can download this manual as an epub `here <https://docs.krita.org/en/"
"epub/KritaManual.epub>`_."
msgstr ""
"Ви можете отримати цей підручник у форматі ePub `тут <https://docs.krita.org/"
"uk_UA/epub/KritaManual.epub>`_"

#: ../../index.rst:33
msgid ":ref:`user_manual`"
msgstr ":ref:`user_manual`"

#: ../../index.rst:33
msgid ":ref:`tutorials`"
msgstr ":ref:`tutorials`"

#: ../../index.rst:35
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""
"Відкрийте для себе можливості Krita за допомогою інтернет-підручника. Його "
"настанови допоможуть вам у переході з інших програм на використання Krita."

#: ../../index.rst:35
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"За допомогою створених розробниками та користувачами підручників навчіться "
"користуванню Krita на прикладах."

#: ../../index.rst:41
msgid ":ref:`getting_started`"
msgstr ":ref:`getting_started`"

#: ../../index.rst:41
msgid ":ref:`reference_manual`"
msgstr ":ref:`reference_manual`"

#: ../../index.rst:43
msgid "New to Krita and don't know where to start?"
msgstr "Робите перші кроки у Krita і ще не знаєте з чого почати?"

#: ../../index.rst:43
msgid "A quick run-down of all of the tools that are available"
msgstr "Швидкий огляд усіх доступних інструментів"

#: ../../index.rst:48
msgid ":ref:`general_concepts`"
msgstr ":ref:`general_concepts`"

#: ../../index.rst:48
msgid ":ref:`faq`"
msgstr ":ref:`faq`"

#: ../../index.rst:50
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""
"Ознайомлення із загальними художніми та технологічними поняттями, які не є "
"специфічними лише для Krita."

#: ../../index.rst:50
msgid ""
"Find answers to the most common questions about Krita and what it offers."
msgstr ""
"Знайдіть відповіді на найпоширеніші питання щодо Krita та можливостей "
"програми."

#: ../../index.rst:55
msgid ":ref:`resources_page`"
msgstr ":ref:`resources_page`"

#: ../../index.rst:55
msgid ":ref:`genindex`"
msgstr ":ref:`genindex`"

#: ../../index.rst:57
msgid ""
"Textures, brush packs, and python plugins to help add variety to your "
"artwork."
msgstr ""
"Текстури, пакети пензлів та додатки мовою Python допоможуть вам додати "
"варіативності у ваші твори."

#: ../../index.rst:57
msgid "An index of the manual for searching terms by browsing."
msgstr "Покажчик підручника для пошуку термінів."
