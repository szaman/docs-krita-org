# Translation of docs_krita_org_reference_manual___preferences___tablet_settings.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___tablet_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/preferences/tablet_settings.rst:1
msgid "Configuring the tablet in Krita."
msgstr "Налаштовування планшета у Krita."

#: ../../reference_manual/preferences/tablet_settings.rst:12
#: ../../reference_manual/preferences/tablet_settings.rst:21
msgid "Tablet"
msgstr "Планшет"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Preferences"
msgstr "Налаштування"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/preferences/tablet_settings.rst:12
msgid "Pressure Curve"
msgstr "Крива тиску"

#: ../../reference_manual/preferences/tablet_settings.rst:17
msgid "Tablet Settings"
msgstr "Параметри планшета"

#: ../../reference_manual/preferences/tablet_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Tablet_Settings.png"

#: ../../reference_manual/preferences/tablet_settings.rst:22
msgid ""
"Input Pressure Global Curve : This is the global curve setting that your "
"tablet will use in Krita. The settings here will make your tablet feel soft "
"or hard globally."
msgstr ""
"Загальна крива вхідних даних тиску: це загальна крива, яку ваш планшет "
"використовує у Krita. Вказані тут параметри можуть зробити поведінку вашого "
"планшета «м'якою» або «жорсткою» на загальному рівні."

#: ../../reference_manual/preferences/tablet_settings.rst:24
msgid ""
"Some tablet devices don't tell us whether the side buttons on a stylus. If "
"you have such a device, you can try activate this workaround. Krita will try "
"to read right and middle-button clicks as if they were coming from a mouse "
"instead of a tablet. It may or may not work on your device (depends on the "
"tablet driver implementation). After changing this option Krita should be "
"restarted."
msgstr ""
"Деякі пристрої планшетів не повідомляють, чи є на стилі бічні кнопки. Якщо "
"ви є власником саме такого пристрою, ви можете спробувати цей спосіб. Krita "
"спробує читати клацання правою та середньою кнопками миші так, наче вони "
"надходять саме від миші, а не від планшета. Це може спрацювати для вашого "
"пристрою, а може і не спрацювати (залежить від реалізації драйвера до "
"планшета). Після зміни значення цього параметра, Krita слід перезапустити."

#: ../../reference_manual/preferences/tablet_settings.rst:26
msgid "Use Mouse Events for Right and Middle clicks."
msgstr "Використовувати події миші для клацання правою та середньою кнопками."

#: ../../reference_manual/preferences/tablet_settings.rst:29
msgid "On Windows 8 or above only."
msgstr "Лише для Windows 8 і новіших версій."

#: ../../reference_manual/preferences/tablet_settings.rst:31
msgid "WinTab"
msgstr "WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:32
msgid ""
"Use the WinTab API to receive tablet pen input. This is the API being used "
"before Krita 3.3. This option is recommended for most Wacom tablets."
msgstr ""
"Використовувати для отримання даних від пера планшета програмний інтерфейс "
"WinTab. Це програмний інтерфейс, який використовувався у версіях Krita до "
"3.3. Рекомендуємо цей варіант для більшості планшетів Wacom."

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "For Krita 3.3 or later:Tablet Input API"
msgstr ""
"Для Krita 3.3 та новіших версій: Програмний інтерфейс введення з планшета"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid "Windows 8+ Pointer Input"
msgstr "Введення з вказівника Windows 8+"

#: ../../reference_manual/preferences/tablet_settings.rst:34
msgid ""
"Use the Pointer Input messages to receive tablet pen input. This option "
"depends on Windows Ink support from the tablet driver. This is a relatively "
"new addition so it's still considered to be experimental, but it should work "
"well enough for painting. You should try this if you are using an N-Trig "
"device (e.g. recent Microsoft Surface devices) or if your tablet does not "
"work well with WinTab."
msgstr ""
"Скористайтеся повідомленнями Pointer Input для отримання вхідних даних пера "
"планшета. Наявність цього пункту залежить від підтримки Windows Ink у "
"драйвері планшета. Це відносно нова можливість, отже, вона все ще вважається "
"експериментальною, але вона має працювати достатньо добре для малювання. Вам "
"слід спробувати скористатися нею, якщо ви користуєтеся пристроєм N-Trig "
"(наприклад нещодавньою версією пристроїв Microsoft Surface) або якщо ваш "
"планшет не працює належним чином з WinTab."

#: ../../reference_manual/preferences/tablet_settings.rst:37
msgid "Advanced Tablet Settings for WinTab"
msgstr "Додаткові параметри планшета для WinTab"

#: ../../reference_manual/preferences/tablet_settings.rst:41
msgid ".. image:: images/preferences/advanced-settings-tablet.png"
msgstr ".. image:: images/preferences/advanced-settings-tablet.png"

#: ../../reference_manual/preferences/tablet_settings.rst:42
msgid ""
"When using multiple monitors or using a tablet that is also a screen, Krita "
"will get conflicting information about how big your screen is, and sometimes "
"if it has to choose itself, there will be a tablet offset. This window "
"allows you to select the appropriate screen resolution."
msgstr ""
"Якщо ви працюєте з декількома моніторами або використовуєте планшет, який є "
"одночасно екраном комп'ютера, Krita отримуватиме конфліктні відомості щодо "
"розмірів вашого екрана і, іноді, якщо програмі доводиться вибирати самій, "
"з'являється певний відступ під час дій, для яких використовується планшет. "
"За допомогою цього вікна налаштовування ви можете вибрати належні параметри "
"роздільності екрана."

#: ../../reference_manual/preferences/tablet_settings.rst:44
msgid "Use Information Provided by Tablet"
msgstr "Використовувати дані, які надано планшетом"

#: ../../reference_manual/preferences/tablet_settings.rst:45
msgid "Use the information as given by the tablet."
msgstr "Використовувати дані, які надано планшетом."

#: ../../reference_manual/preferences/tablet_settings.rst:46
msgid "Map to entire virtual screen"
msgstr "Пов'язати з усім віртуальним екраном"

#: ../../reference_manual/preferences/tablet_settings.rst:47
msgid "Use the information as given by Windows."
msgstr "Використовувати дані, які надано Windows."

#: ../../reference_manual/preferences/tablet_settings.rst:49
msgid ""
"Type in the numbers manually. Use this when you have tried the other "
"options. You might even need to do trial and error if that is the case, but "
"at the least you can configure it."
msgstr ""
"Введіть числа вручну. Скористайтеся цим варіантом, якщо інші варіанти не "
"спрацьовують. Можливо, вам навіть доведеться трохи використати метод спроб і "
"помилок, але, врешті, це дасть вам можливість таки налаштувати програму."

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid "Map to Custom Area"
msgstr "Пов'язати із нетиповою ділянкою"

#: ../../reference_manual/preferences/tablet_settings.rst:51
msgid ""
"If you have a dual monitor setup and only the top half of the screen is "
"reachable, you might have to enter the total width of both screens plus the "
"double height of your monitor in this field."
msgstr ""
"Якщо ви працюєте за двома моніторами і можете користуватися лише верхньою "
"частиною екрана, у цьому полі, ймовірно, слід вказати загальну ширину обох "
"екранів плюс подвоєну висоту зображення на вашому моніторі."

#: ../../reference_manual/preferences/tablet_settings.rst:55
msgid ""
"To access this dialog in Krita versions older than 4.2, you had to do the "
"following:"
msgstr ""
"Щоб отримати доступ до цього діалогового вікна у версіях Krita до 4.2, вам "
"слід виконати такі дії:"

#: ../../reference_manual/preferences/tablet_settings.rst:57
msgid "Put your stylus away from the tablet."
msgstr "Приберіть стило подалі від планшета."

#: ../../reference_manual/preferences/tablet_settings.rst:58
msgid ""
"Start Krita without using a stylus, that is using a mouse or a keyboard."
msgstr ""
"Запустіть Krita без використання стила, тобто з використання миші або "
"клавіатури."

#: ../../reference_manual/preferences/tablet_settings.rst:59
msgid "Press the :kbd:`Shift` key and hold it."
msgstr "Натисніть і утримуйте клавішу :kbd:`Shift`."

#: ../../reference_manual/preferences/tablet_settings.rst:60
msgid "Touch a tablet with your stylus so Krita would recognize it."
msgstr "Доторкніться стилом до планшета так, щоб Krita розпізнала планшет."

#: ../../reference_manual/preferences/tablet_settings.rst:62
msgid ""
"If adjusting this doesn't work, and if you have a Wacom tablet, an offset in "
"the canvas can be caused by a faulty Wacom preference file which is not "
"removed or replaced by reinstalling the drivers."
msgstr ""
"Якщо це не спрацює і ви користуєтеся планшетом Wacom, зсув на полотні може "
"бути спричинено помилками у файлі налаштувань Wacom, який не було вилучено "
"або замінено під час перевстановлення драйверів."

#: ../../reference_manual/preferences/tablet_settings.rst:64
msgid ""
"To fix it, use the “Wacom Tablet Preference File Utility” to clear all the "
"preferences. This should allow Krita to detect the correct settings "
"automatically."
msgstr ""
"Щоб виправити помилку, скористайтеся «Wacom Tablet Preference File Utility» "
"для вилучення усіх налаштувань. Після цього Krita зможе визначити належні "
"параметри роботи автоматично."

#: ../../reference_manual/preferences/tablet_settings.rst:67
msgid ""
"Clearing all wacom preferences will reset your tablet's configuration, thus "
"you will need to recalibrate/reconfigure it."
msgstr ""
"Відновлення початкових значень усіх налаштувань wacom призведе до тощо, що "
"налаштування вашого планшета буде скинуто до типових, отже вам доведеться "
"повторно виконати процедуру калібрування і налаштовування."

#: ../../reference_manual/preferences/tablet_settings.rst:70
msgid "Tablet Tester"
msgstr "Тестування планшета…"

#: ../../reference_manual/preferences/tablet_settings.rst:74
msgid ""
"This is a special feature for debugging tablet input. When you click on it, "
"it will open a window with two sections. The left section is the **Drawing "
"Area** and the right is the **Text Output**."
msgstr ""
"Це особлива можливість для діагностики вхідних даних планшета. Якщо "
"натиснути цей пункт, буде відкрито вікно із двома розділами. Лівий розділ "
"матиме назву **Область малювання**, а права — **Виведення тексту**."

#: ../../reference_manual/preferences/tablet_settings.rst:76
msgid ""
"If you draw over the Drawing Area, you will see a line appear. If your "
"tablet is working it should be both a red and blue line."
msgstr ""
"Під час малювання на ділянці малювання з'явиться лінія. Якщо ваш планшет "
"працює як слід, це будуть дві лінії — червона і синя."

#: ../../reference_manual/preferences/tablet_settings.rst:78
msgid ""
"The red line represents mouse events. Mouse events are the most basic events "
"that Krita can pick up. However, mouse events have crude coordinates and "
"have no pressure sensitivity."
msgstr ""
"Червона лінія відповідає подіям миші. Події миші — найбільш базові події, "
"які може реєструвати Krita. Втім, події миші дають не дуже точні координати "
"і не передають дані щодо чутливості до тиску."

#: ../../reference_manual/preferences/tablet_settings.rst:80
msgid ""
"The blue line represents the tablet events. The tablet events only show up "
"when Krita can access your tablet. These have more precise coordinates and "
"access to sensors like pressure sensitivity."
msgstr ""
"Синя лінія відповідає подіям планшета. Події планшета буде показано, лише "
"якщо Krita має доступ до планшета. Ці події дають точніші координати і дають "
"доступ до даних щодо чутливості до тиску."

#: ../../reference_manual/preferences/tablet_settings.rst:84
msgid ""
"If you have no blue line when drawing on the lefthand drawing area, Krita "
"cannot access your tablet. Check out the :ref:`page on drawing tablets "
"<drawing_tablets>` for suggestions on what is causing this."
msgstr ""
"Якщо ви не бачите синьої лінії під час малювання на лівій області малювання, "
"Krita не може отримати доступ до даних вашого планшета. Ознайомтеся із :ref:"
"`розділом щодо малювання на планшеті <drawing_tablets>`, щоб дізнатися "
"більше про те, як усунути ці проблеми."

#: ../../reference_manual/preferences/tablet_settings.rst:86
msgid ""
"When you draw a line, the output on the right will show all sorts of text "
"output. This text output can be attached to a help request or a bug report "
"to figure out what is going on."
msgstr ""
"Під час малювання лінії у області виведення праворуч буде показано усі "
"різновиди виведених текстових даних. Ці текстові дані може бути долучено до "
"запиту щодо допомоги або звіту щодо вади. Вони допомагають розібратися із "
"тим, що відбувається."

#: ../../reference_manual/preferences/tablet_settings.rst:89
msgid "External Links"
msgstr "Зовнішні посилання"

#: ../../reference_manual/preferences/tablet_settings.rst:91
msgid ""
"`David Revoy wrote an indepth guide on using this feature to maximum "
"advantage. <https://www.davidrevoy.com/article182/calibrating-wacom-stylus-"
"pressure-on-krita>`_"
msgstr ""
"`Давід Ревуа написав докладні настанови щодо використання цієї можливості із "
"максимальною користю. <https://www.davidrevoy.com/article182/calibrating-"
"wacom-stylus-pressure-on-krita>`_"
