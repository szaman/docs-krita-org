# Translation of docs_krita_org_reference_manual___dockers___onion_skin.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___onion_skin\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:29+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/dockers/onion_skin.rst:1
msgid "Overview of the onion skin docker."
msgstr "Огляд бічної панелі кальки."

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Animation"
msgstr "Анімація"

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Onion Skin"
msgstr "Калька"

#: ../../reference_manual/dockers/onion_skin.rst:15
msgid "Onion Skin Docker"
msgstr "Бічна панель кальки"

#: ../../reference_manual/dockers/onion_skin.rst:18
msgid ".. image:: images/dockers/Onion_skin_docker.png"
msgstr ".. image:: images/dockers/Onion_skin_docker.png"

#: ../../reference_manual/dockers/onion_skin.rst:19
msgid ""
"To make animation easier, it helps to see both the next frame as well as the "
"previous frame sort of layered on top of the current. This is called *onion-"
"skinning*."
msgstr ""
"Для спрощення процесу анімації корисно бачити одразу наступний кадр і "
"попередній кадр певними чином накладені на поточний кадр. Така композиція "
"подібна до декількох листків кальки із зображенням, накладених один на оден."

#: ../../reference_manual/dockers/onion_skin.rst:22
msgid ".. image:: images/dockers/Onion_skin_01.png"
msgstr ".. image:: images/dockers/Onion_skin_01.png"

#: ../../reference_manual/dockers/onion_skin.rst:23
msgid ""
"Basically, they are images that represent the frames before and after the "
"current frame, usually colored or tinted."
msgstr ""
"Якщо говорити просто, кальки — зображення, які відповідають кадрам до і "
"після поточного. Зазвичай, зображення на кальках відповідним чином "
"розфарбовуються або їм надається певний відтінок."

#: ../../reference_manual/dockers/onion_skin.rst:25
msgid ""
"You can toggle them by clicking the lightbulb icon on a layer that is "
"animated (so, has frames), and isn’t fully opaque. (Krita will consider "
"white to be white, not transparent, so don’t animated on an opaque layer if "
"you want onion skins.)"
msgstr ""
"Ви можете вмикати або вимикати кальки клацанням на піктограмі лампи поряд із "
"пунктом анімованого шару (шару, який містить кадри), який не є повністю "
"непрозорим. (Krita вважає білий колір білим, а не прозорим, тому не анімуйте "
"непрозорий шар, якщо ви хочете використовувати можливості кальки.)"

#: ../../reference_manual/dockers/onion_skin.rst:29
msgid ""
"Since 4.2 onion skins are disabled on layers whose default pixel is fully "
"opaque. These layers can currently only be created by using :guilabel:"
"`background as raster layer` in the :guilabel:`content` section of the new "
"image dialog. Just don't try to animate on a layer like this if you rely on "
"onion skins, instead make a new one."
msgstr ""
"Починаючи з версії 4.2, кальки вимкнено для шарів, типовий піксель яких є "
"повністю непрозорим. Такі шари у поточній версії можна створити лише за "
"допомогою позначення пункту :guilabel:`Тло як растровий шар` у розділі :"
"guilabel:`Вміст` діалогового вікна створення зображення. Просто не створюйте "
"анімацій на подібних шарах, якщо хочете користуватися кальками."

#: ../../reference_manual/dockers/onion_skin.rst:31
msgid ""
"The term onionskin comes from the fact that onions are semi-transparent. In "
"traditional animation animators would make their initial animations on "
"semitransparent paper on top of an light-table (of the special animators "
"variety), and they’d start with so called keyframes, and then draw frames in "
"between. For that, they would place said keyframes below the frame they were "
"working on, and the light table would make the lines of the keyframes shine "
"through, so they could reference them."
msgstr ""
"Термін «калька» походить від того, що кальки є напівпрозорими. У традиційній "
"анімації аніматори створюють початкові версій анімацій на напівпрозорому "
"папері на столі із підсвічуванням (спеціально призначеним для анімування). "
"Роботу розпочинають із так званих ключових кадрів, потім малюють проміжні "
"кадри. Для цього аніматори підкладають ключові кадри під кадр, над яким вони "
"працюють, а стіл із підсвічуванням забезпечує просвічування ключових кадрів, "
"отже, їх дуже просто використати як еталонні."

#: ../../reference_manual/dockers/onion_skin.rst:33
msgid ""
"Onion-skinning is a digital implementation of such a workflow, and it’s very "
"useful when trying to animate."
msgstr ""
"Воскові кальки у Krita є цифровою реалізацією звичайних кальок. Вони дуже "
"корисні під час анімування."

#: ../../reference_manual/dockers/onion_skin.rst:36
msgid ".. image:: images/dockers/Onion_skin_02.png"
msgstr ".. image:: images/dockers/Onion_skin_02.png"

#: ../../reference_manual/dockers/onion_skin.rst:37
msgid ""
"The slider and the button with zero offset control the master opacity and "
"visibility of all the onion skins. The boxes at the top allow you to toggle "
"them on and off quickly, the main slider in the middle is a sort of ‘master "
"transparency’ while the sliders to the side allow you to control the "
"transparency per keyframe offset."
msgstr ""
"Повзунок і кнопка із нульовим відступом керують основним рівнем непрозорості "
"та видимістю усіх шарів кальки. За допомогою стовпчиків вище ви можете "
"швидко вмикати або вимикати кальку для відповідних кадрів. Основний повзунок "
"посередині визначає певний рівень основної прозорості, а повзунки обабіч "
"нього надають вам змогу керувати прозорістю кальки окремих кадрів."

#: ../../reference_manual/dockers/onion_skin.rst:39
msgid ""
"Tint controls how strongly the frames are tinted, the first screen has 100%, "
"which creates a silhouette, while below you can still see a bit of the "
"original colors at 50%."
msgstr ""
"За допомогою повзунка відтінку можна керувати інтенсивністю відтінку кадрів. "
"Перший кадр матиме 100% відтінок, який створюватиме чіткий силует, а кальки "
"під ним можуть мати лише частку початкових кольорів, наприклад у 50%."

#: ../../reference_manual/dockers/onion_skin.rst:41
msgid ""
"The :guilabel:`Previous Frame` and :guilabel:`Next Frame` color labels "
"allows you set the colors."
msgstr ""
"Пункти попереднього і наступного кадрів надають вам змогу встановити кольори."
