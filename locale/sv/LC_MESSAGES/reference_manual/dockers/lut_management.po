# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:08+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Set white and black points"
msgstr "Ställ in vita och svarta punkter"

#: ../../reference_manual/dockers/lut_management.rst:1
msgid "Overview of the LUT management docker."
msgstr "Översikt av LUT-hanteringspanelen."

#: ../../reference_manual/dockers/lut_management.rst:11
#: ../../reference_manual/dockers/lut_management.rst:16
msgid "LUT Management"
msgstr "Hantering av uppslagningstabell"

#: ../../reference_manual/dockers/lut_management.rst:11
msgid "Look Up Table"
msgstr "Look Up Table"

#: ../../reference_manual/dockers/lut_management.rst:11
msgid "OCIO"
msgstr "OCIO"

#: ../../reference_manual/dockers/lut_management.rst:19
msgid ".. image:: images/dockers/LUT_Management_Docker.png"
msgstr ".. image:: images/dockers/LUT_Management_Docker.png"

#: ../../reference_manual/dockers/lut_management.rst:20
msgid ""
"The Look Up Table (LUT) Management docker controls the high dynamic range "
"(HDR) painting functionality."
msgstr ""
"LUT (Look Up Table)-hanteringspanelen kontrollerar målningsfunktionen för "
"stort dynamiskt intervall (HDR)."

#: ../../reference_manual/dockers/lut_management.rst:22
msgid "Use OpenColorIO"
msgstr "Använd OpenColorIO"

#: ../../reference_manual/dockers/lut_management.rst:23
msgid ""
"Use Open Color IO instead of Krita's internal color management. Open Color "
"IO is a color management library. It is sometimes referred to as OCIO. This "
"is required as Krita uses OCIO for its HDR functionality."
msgstr ""
"Använd Open Color IO istället för Kritas interna färghantering. Open Color "
"IO är ett färghanteringsbibliotek. Det kallas ibland för OCIO. Det krävs, "
"eftersom Krita använder OCIO för sin HDR-funktion."

#: ../../reference_manual/dockers/lut_management.rst:24
msgid "Color Engine"
msgstr "Färggränssnitt"

#: ../../reference_manual/dockers/lut_management.rst:25
msgid "Choose the engine."
msgstr "Välj gränssnittet."

#: ../../reference_manual/dockers/lut_management.rst:27
msgid "Use an OCIO configuration file from your computer."
msgstr "Använd en OCIO-inställningsfil från datorn."

#: ../../reference_manual/dockers/lut_management.rst:31
msgid "Configuration"
msgstr "Inställning"

#: ../../reference_manual/dockers/lut_management.rst:31
msgid ""
"Some system locals don't allow you to read the configuration files. This is "
"due to a bug in OCIO. If you are using Linux you can fix this. If you start "
"Krita from the terminal with the ``LC_ALL=C krita`` flag set, you should be "
"able to read the configuration files."
msgstr ""
"Landsinställningarna på vissa system tillåter inte att inställningsfilerna "
"läses. Det beror på ett fel i OCIO. Om man använder Linux går det att fixa: "
"Genom att starta Krita från terminalen med ``LC_ALL=C krita``, ska det gå "
"att läsa inställningsfilerna."

#: ../../reference_manual/dockers/lut_management.rst:33
msgid "Input Color Space"
msgstr "Ingående färgrymd"

#: ../../reference_manual/dockers/lut_management.rst:34
msgid "What the color space of the image is. Usually sRGB or Linear."
msgstr "Vad bildens färgrymd är. Oftast sRGB eller Linjär."

#: ../../reference_manual/dockers/lut_management.rst:35
msgid "Display Device"
msgstr "Bildskärmsenhet"

#: ../../reference_manual/dockers/lut_management.rst:36
msgid ""
"The type of device you are using to view the colors. Typically sRGB for "
"computer screens."
msgstr ""
"Enhetstyp som används för att titta på färgerna. Typiskt sRGB för "
"datorbildskärmar."

#: ../../reference_manual/dockers/lut_management.rst:37
msgid "View"
msgstr "Visa"

#: ../../reference_manual/dockers/lut_management.rst:38
msgid "--"
msgstr "--"

#: ../../reference_manual/dockers/lut_management.rst:39
msgid "Components"
msgstr "Komponenter"

#: ../../reference_manual/dockers/lut_management.rst:40
msgid "Allows you to study a single channel of your image with LUT."
msgstr "Tillåter att man betraktar en enskild kanal av bilden med LUT."

#: ../../reference_manual/dockers/lut_management.rst:42
msgid "Exposure"
msgstr "Exponering"

#: ../../reference_manual/dockers/lut_management.rst:42
msgid ""
"Set the general exposure. On 0.0 at default. There's the :kbd:`Y` key to "
"change this on the fly on canvas."
msgstr ""
"Ställer in allmänna exponeringen. Normalt inställd till 0,0. Använd "
"genvägen :kbd:`Y` för att ändra den på duken i farten."

#: ../../reference_manual/dockers/lut_management.rst:44
msgid "Gamma"
msgstr "Gamma"

#: ../../reference_manual/dockers/lut_management.rst:45
msgid ""
"Allows you to set the gamma. This is 1.0 by default. You can set this to "
"change on the fly in canvas shortcuts."
msgstr ""
"Tillåter att man ställer in gamma. Det är normalt 1,0. Man kan ställa in det "
"att ändras på duken i farten med genvägar för duken."

#: ../../reference_manual/dockers/lut_management.rst:46
msgid "Lock color"
msgstr "Lås färg"

#: ../../reference_manual/dockers/lut_management.rst:47
msgid ""
"Locks the color to make sure it doesn't shift when changing exposure. May "
"not be desired."
msgstr ""
"Låser färgen för att säkerställa att den inte ändras när exponeringen "
"ändras. Kanske inte är önskvärt."

#: ../../reference_manual/dockers/lut_management.rst:49
msgid ""
"This allows you to set the maximum and minimum brightness of the image, "
"which'll adjust the exposure and gamma automatically to this."
msgstr ""
"Tillåter att man ställer in bildens maximala och minimala ljusstyrka, vilket "
"automatiskt justerar exponering och gamma i enlighet med det."
