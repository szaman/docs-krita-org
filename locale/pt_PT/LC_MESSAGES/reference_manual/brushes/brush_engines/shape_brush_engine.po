# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 18:04+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: chemy pixelizada icons image Al optionexperiment\n"
"X-POFile-SpellExtra: blendingmodes images shapebrush ref Chemy\n"

#: ../../<generated>:1
msgid "Hard Edge"
msgstr "Aresta Forte"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:1
msgid "The Shape Brush Engine manual page."
msgstr "A página de manual do Motor de Pincéis com Formas."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:16
msgid "Shape Brush Engine"
msgstr "Motor de Pincel com Formas"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Experiment Brush Engine"
msgstr "Motor de Pincel de Experiência"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Al.Chemy"
msgstr "Al.Chemy"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:19
msgid ".. image:: images/icons/shapebrush.svg"
msgstr ".. image:: images/icons/shapebrush.svg"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:20
msgid "An Al.chemy inspired brush-engine. Good for making chaos with!"
msgstr ""
"Um motor de pincéis inspirado no Al.chemy. Bom para criar alguma confusão "
"com ele!"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:23
msgid "Parameters"
msgstr "Parâmetros"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:25
msgid ":ref:`option_experiment`"
msgstr ":ref:`option_experiment`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:26
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:31
msgid "Experiment Option"
msgstr "Opção de Experiência"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:33
msgid "Speed"
msgstr "Velocidade"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:34
msgid ""
"This makes the outputted contour jaggy. The higher the speed, the jaggier."
msgstr ""
"Isto fará com que o contorno seja tremido. Quanto maior a velocidade, mais "
"tremido será."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:35
msgid "Smooth"
msgstr "Suave"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:36
msgid ""
"Smoothens the output contour. This slows down the brush, but the higher the "
"smooth, the smoother the contour."
msgstr ""
"Suaviza o contorno resultante. Isto baixa mais o pincel, mas quanto maior a "
"suavização, mais suave será o contorno."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:37
msgid "Displace"
msgstr "Deslocar"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:38
msgid ""
"This displaces the shape. The slow the movement, the higher the displacement "
"and expansion. Fast movements shrink the shape."
msgstr ""
"Isto desloca a forma. Quanto mais lento o movimento, maior será a deslocação "
"e expansão. Os movimentos rápidos reduzem a forma."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:39
msgid "Winding Fill"
msgstr "Preenchimento à Passagem"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:40
msgid ""
"This gives you the option to use a 'non-zero' fill rules instead of the "
"'even-odd' fill rule, which means that where normally crossing into the "
"shape created transparent areas, it now will not."
msgstr ""
"Isto dá-lhe a opção para usar as regras de preenchimento 'diferente-de-zero' "
"em vez da regra de preenchimento 'par-ímpar', o que significa que, onde "
"normalmente se cruzaria com as áreas transparentes criadas pela forma, agora "
"deixaria de o fazer."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:42
msgid "Removes the anti-aliasing, to get a pixelized line."
msgstr "Remove a suavização para obter uma linha pixelizada."
